function onlyNumbers(event) {
    var regex = new RegExp("^[0-9]*$")
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault()
    }
}

function showBadge(boolean){
	 chrome.browserAction.setBadgeBackgroundColor({color: '#4688F1'});
		switch (boolean) {
		  case true:  
		  chrome.browserAction.setBadgeText({text: 'ON'});
		  break;
		  case false:
		  chrome.browserAction.setBadgeText({text: ''});
		}
}

function startRefreshByInterval() {
    chrome.alarms.clear('RefreshByInterval')
    chrome.alarms.create('RefreshByInterval', {
        periodInMinutes: +refreshInterval.value
    })
	showBadge(true);
}
var refreshInterval = document.getElementById("refreshInterval");
var intervalSwitch = document.getElementById("intervalSwitch");
var errorSwitch = document.getElementById("errorSwitch");
var tabSwitch = document.getElementById("tabSwitch");


chrome.storage.local.get(function (data) {
    intervalSwitch.checked = data.intervalSwitch;
	errorSwitch.checked = data.errorSwitch;
    tabSwitch.checked = data.tabSwitch;
	    if (data.refreshInterval !== undefined)
        refreshInterval.value = data.refreshInterval
});

intervalSwitch.addEventListener('change', function () {
    chrome.storage.local.set({
        intervalSwitch: intervalSwitch.checked
    })
    if (intervalSwitch.checked) {
        startRefreshByInterval()
    } else {
        chrome.alarms.clear('RefreshByInterval')
		showBadge(intervalSwitch.checked)
    }
});

refreshInterval.addEventListener("input", function () {
    chrome.storage.local.set({
        refreshInterval: refreshInterval.value
    });
    if (intervalSwitch.checked) {
        startRefreshByInterval()
    };
});
refreshInterval.addEventListener("keypress", onlyNumbers);

errorSwitch.addEventListener('change', function () {
    chrome.storage.local.set({
        errorSwitch: errorSwitch.checked
    })
	showBadge(errorSwitch.checked);
});

tabSwitch.addEventListener('change', function () {
    chrome.storage.local.set({
        tabSwitch: tabSwitch.checked
    });
	showBadge(tabSwitch.checked);
});

