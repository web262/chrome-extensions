function onErrorOccurred(details) {
    chrome.storage.local.get(function(data) {
        if (data.errorSwitch && details.error != 'net::ERR_ABORTED') {
            // alert(details.error)
            chrome.tabs.reload()
        }
    });
}

function onTabCrash() {
	var tabSuccessCount = {};
	
function checkActive(tabs) {
	var tabsLength = tabs.length;

    while (tabsLength--) {
        (function() {
            var thisTab = tabs[tabsLength];

            
            if (( thisTab.url.substring(0, 4) == "http" || thisTab.url.substring(0, 4) == "file" ) && thisTab.status == "complete") {

                
                chrome.tabs.executeScript(thisTab.id, {
                    code: "null;"
                }, function(result) {
                    
					if (chrome.runtime.lastError && chrome.runtime.lastError.message == "The tab was closed.") {
                        console.log("Crashed: ", thisTab.title, thisTab.id);
                        
                        
                        if (tabSuccessCount[thisTab.id] > 0) {
                            console.log("Reloading: ", thisTab.title, thisTab.id);
                            chrome.tabs.reload(thisTab.id);
                        }
                    } else {
                        
                        tabSuccessCount[thisTab.id] = tabSuccessCount[thisTab.id] || 0;
                        tabSuccessCount[thisTab.id]++;
                    }
                });
            }
        }).call();
    }
}


setInterval(function() {
    chrome.tabs.query({}, checkActive);
}, 1000);


chrome.tabs.onUpdated.addListener(tabChanged);
chrome.tabs.onRemoved.addListener(tabChanged);


function tabChanged(tabId, changeInfo, tab) {
    console.log("Resetting Stats: ", tabId);
    tabSuccessCount[tabId] = 0;
} chrome.extension.onRequest.addListener(function(request, sender) {
    chrome.tabs.update(sender.tab.id, {url: request.redirect});
});
}

function refreshByTabCrash() {
	 chrome.storage.local.get(function(data) {
		if (data.tabSwitch){
		onTabCrash();
		};			
	 });
};

chrome.alarms.onAlarm.addListener(function(alarm) {
    chrome.tabs.reload()
});
chrome.webNavigation.onErrorOccurred.addListener(onErrorOccurred);
refreshByTabCrash();

